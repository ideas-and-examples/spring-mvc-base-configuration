package fef.com.wha.test.mypackage;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import fef.com.wha.test.formatter.FormatterType;

@Controller
public class HelloController {
	
	@Autowired
    private FormatterType formatter;
	
    @GetMapping({"/", "/hello"})
    public String hello(@RequestParam(value="name", required=false, defaultValue="World") String name, Model model) {
    	
    	System.out.println(formatter.toString());
    	String myStr = formatter.value();
    	System.out.println(myStr);
        model.addAttribute("name", name);
        return "hello";
    }
}